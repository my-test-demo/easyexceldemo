package com.example.demo.test;


import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.util.PublicSuffixMatcher;
import org.apache.http.conn.util.PublicSuffixMatcherLoader;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.net.Inet4Address;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

@Component
public class BaiduApiUtils {

    private static String baiduApi = "http://api.map.baidu.com/location/ip?ip=IP&ak=AK&coor=COOR";
    private static String baiduAK = "WUPGAouSp0YkG7yA6upSBlnL";
    private static String cool = "gcj02";
    private static RequestConfig REQUEST_CONFIG;
    private final static String ZERO = "0";


//    @Value("${baidu.ip.location.api}")
//    public void setBaiduApi(String baiduApi) {
//        BaiduApiUtils.baiduApi = baiduApi;
//    }
//
//    @Value("${baidu.ak}")
//    public void setBaiduAK(String baiduAK) {
//        BaiduApiUtils.baiduAK = baiduAK;
//    }
//
//    @Value("${baidu.coor}")
//    public void setCool(String cool) {
//        BaiduApiUtils.cool = cool;
//    }

    public static String sendHttpGet(String url) {
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;
        org.apache.http.HttpEntity entity = null;
        String result = null;

        try {
            PublicSuffixMatcher publicSuffixMatcher = PublicSuffixMatcherLoader.load(new URL(httpGet.getURI().toString()));
            DefaultHostnameVerifier hostnameVerifier = new DefaultHostnameVerifier(publicSuffixMatcher);
            httpClient = HttpClients.custom().setSSLHostnameVerifier(hostnameVerifier).build();
//            httpGet.setConfig(REQUEST_CONFIG);
            response = httpClient.execute(httpGet);
            entity = response.getEntity();
            result = EntityUtils.toString(entity, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getIpLocation(String ip) {
        String httpUrl = "";
//        "https://www.cnblogs.com/coprince/p/8692972.html"
//        httpUrl = "http://api.map.baidu.com/location/ip?ip=192.168.202.91&ak=l6mdbkQRLz5NdTir2HDZO4WsAZMFKqK8&coor=gcj02";
//        "http://api.map.baidu.com/location/ip?ak=l6mdbkQRLz5NdTir2HDZO4WsAZMFKqK8";
//        String url = "http://api.map.baidu.com/geocoder/v2/?address=陕西省西安市&output=json&ak=l6mdbkQRLz5NdTir2HDZO4WsAZMFKqK8";
        httpUrl = baiduApi.replace("IP", ip);
        httpUrl = httpUrl.replace("AK", baiduAK);
        httpUrl = httpUrl.replace("COOL", cool);
        System.out.println(httpUrl);

        return BaiduApiUtils.sendHttpGet(httpUrl);

    }


    public static HashMap<String, String> getPoint(String address) {
        String url = "http://api.map.baidu.com/geocoder/v2/?address=ADDR&output=json&ak=AK";

        url = url.replace("ADDR", address).replace("AK", baiduAK);
        String res = sendHttpGet(url);
        HashMap<String, String> map = new HashMap<>();
        if (!StringUtils.isEmpty(res)) {
            JSONObject jsonObject = JSONObject.parseObject(res);
            Integer status = jsonObject.getInteger("status");
            if (status.equals(0)) {
                JSONObject result = jsonObject.getJSONObject("result");
                JSONObject location = result.getJSONObject("location");
                String lng = location.getString("lng");
                String lat = location.getString("lat");
                String level = location.getString("level");

                map.put("lng", lng);
                map.put("lat", lat);
                map.put("level", level);
            }
        }
        return map;
    }

    /**
     * 地址解析聚合
     *
     * @param address
     * @return
     */
    public static HashMap<String, String> addrAggregation(String address) {
        HashMap<String, String> map = new HashMap<>();
        String baiduMapApi = "api.map.baidu.com/address_analyzer/v1?";
        String url = "http://" + baiduMapApi + "address=" + address + "&ak=" + baiduAK;
        String resultData = "";
        if (!StringUtils.isEmpty(address)) {
            try {
                System.out.println("url:" + url);
                resultData = sendHttpGet(url);
                JSONObject jsonObject = JSONObject.parseObject(resultData);
                System.out.println("jsonObject:" + jsonObject);
                if (ZERO.equals(jsonObject.getString("status"))) {
                    JSONObject resultObject = jsonObject.getJSONObject("result");
                    if (!StringUtils.isEmpty(resultObject)) {
                        Map<String,String> maps = JSONObject.parseObject(resultObject.toJSONString(), new TypeReference<Map<String, String>>(){});
                        System.out.println("vars:"+maps);

//                        String province = resultObject.getString("province");
//                        String provinceCode = resultObject.getString("province_code");
//                        String city = resultObject.getString("city");
//                        String cityCode = resultObject.getString("city_code");
//                        String county = resultObject.getString("county");
//                        String countyCode = resultObject.getString("county_code");
//                        String town = resultObject.getString("town");
//                        String townCode = resultObject.getString("town_code");
//                        String road = resultObject.getString("road");
//                        String roadCode = resultObject.getString("road_code");
//
//                        map.put("province",province);
//                        map.put("provinceCode",provinceCode);
//                        map.put("city",city);
//                        map.put("cityCode",cityCode);
//                        map.put("county",county);
//                        map.put("countyCode",countyCode);
//                        map.put("town",town);
//                        map.put("townCode",townCode);
//                        map.put("road",road);
//                        map.put("roadCode",roadCode);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }




    

    public static void main(String[] args) {


//        System.out.println(BaiduApiUtils.getPoint("陕西省西安市高新区天谷八路云水一路中软国际"));
        //{lng=108.95144045637756,lat=34.220635239985899, level=null}

        System.out.println(BaiduApiUtils.addrAggregation("陕西省西安市高新区天谷八路云水一路交叉口中软国际"));
        //"address_xy":[12128550.84,4034214.31]
    }
}
