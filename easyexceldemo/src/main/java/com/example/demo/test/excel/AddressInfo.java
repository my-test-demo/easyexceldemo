package com.example.demo.test.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

@Data
public class AddressInfo extends BaseRowModel {
    @ExcelProperty(value ={"address"},index = 0)
    private String address;
    @ExcelProperty(value ={"province"},index = 1)
    private String province;
    @ExcelProperty(value ={"city"},index = 2)
    private String city;
    @ExcelProperty(value ={"county"},index = 3)
    private String county;


}
