package com.example.demo.test.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ExcelListener extends AnalysisEventListener {

    @Getter
    @Setter
    private List<Object> datas = new ArrayList<>();

    @Override
    public void invoke(Object obj, AnalysisContext analysisContext) {
        log.info("current num：{}", analysisContext.getCurrentRowNum());
        log.info("object：{}", obj);
        datas.add(obj);

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//        datas.clear();
    }
}
