package com.example.demo.test.excel;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.BaseRowModel;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

@Slf4j
public class ExcelUtil {

    /**
     * @param filePath
     * @param clazz
     * @param excelType
     * @return
     */
    public static List<Object> readExcel(String filePath, Class<? extends BaseRowModel> clazz, ExcelTypeEnum excelType) {
        ExcelListener listener = null;
        InputStream in = null;
        try {
            listener = new ExcelListener();
            in = new FileInputStream(filePath);
            ExcelReader excelReader = new ExcelReader(in, excelType, null, listener);
            excelReader.read(new Sheet(1, 1, clazz));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return listener.getDatas();

    }

    /**
     * @param response
     * @param datas
     * @param excelTypeEnum
     */

    public static void writeExcel(OutputStream outputStream, List<? extends BaseRowModel> datas, ExcelTypeEnum excelTypeEnum,Sheet sheet)  {
        ExcelWriter writer = new ExcelWriter(outputStream, excelTypeEnum,true);
        writer.write(datas, sheet);
        writer.finish();
    }

//    public static void writeExcel(OutputStream response, List<? extends BaseRowModel> datas, ExcelTypeEnum excelTypeEnum, Sheet sheet) {
//        try {
//            response.setContentType("multipart/form-data");
//            response.setCharacterEncoding("utf-8");
//            response.setHeader("Content-disposition", "attachment;filename=" + "default" + excelTypeEnum.getValue());
//            ServletOutputStream out = response.getOutputStream();
//            ExcelWriter writer = new ExcelWriter(out, excelTypeEnum, true);
//            writer.write(datas, sheet);
//            writer.finish();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public static List<Object> readMoreThan1000RowBySheet(String filePath) {
        if (!StringUtils.hasText(filePath)) {
            return null;
        }
        Sheet sheet = new Sheet(1, 0);
        sheet.setSheetName("Sheet1");
        InputStream fileStream = null;
        try {
            fileStream = new FileInputStream(filePath);
            ExcelListener excelListener = new ExcelListener();
            EasyExcelFactory.readBySax(fileStream, sheet, excelListener);
            return excelListener.getDatas();
        } catch (FileNotFoundException e) {
            log.error("找不到文件或文件路径错误, 文件：{}", filePath);
        } finally {
            try {
                if (fileStream != null) {
                    fileStream.close();
                }
            } catch (IOException e) {
                log.error("excel文件读取失败, 失败原因：{}", e);
            }
        }
        return null;
    }


}
