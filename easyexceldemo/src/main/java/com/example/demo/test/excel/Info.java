package com.example.demo.test.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class Info extends BaseRowModel {

    @ExcelProperty(value ={"姓名"},index = 0)
    private String name;

    @ExcelProperty(value ={"创建时间"},index =1,format = "yyyy-MM-dd hh:mm:ss")
    private Date date;

    @ExcelProperty(value ={"From"},index = 2)
    private String from;

    @ExcelProperty(value ={"To"},index = 3)
    private String to;

    @ExcelProperty(value ={"数量"},index = 4)
    private String quantity;

}
