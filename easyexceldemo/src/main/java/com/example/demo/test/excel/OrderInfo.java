package com.example.demo.test.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

@Data
public class OrderInfo extends BaseRowModel {

    @ExcelProperty(value ={"receive","receive_prov_name"},index = 0)
    private String recipientProvName;
    @ExcelProperty(value ={"receive","receive_city_name"},index = 1)
    private String recipientCityName;
    @ExcelProperty(value ={"receive","receive_county_name"},index = 2)
    private String recipientAreaName;
    @ExcelProperty(value ={"receive","receive_address"},index = 3)
    private String recipientAddress;
    @ExcelProperty(value ={"sender","sender_prov_name"},index = 4)
    private String senderProvName;
    @ExcelProperty(value ={"sender","sender_city_name"},index = 5)
    private String senderCityName;
    @ExcelProperty(value ={"sender","sender_county_name"},index = 6)
    private String senderAreaName;
    @ExcelProperty(value ={"sender","sender_address"},index = 7)
    private String senderAddress;
}