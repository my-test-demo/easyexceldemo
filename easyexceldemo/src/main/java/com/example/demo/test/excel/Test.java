package com.example.demo.test.excel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import io.lettuce.core.internal.LettuceLists;
import org.apache.poi.ss.usermodel.CellStyle;

import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Test {

    public List<AddressInfo> getAddresss() {
        String username = "otp_addr";
        String password = "RFgktIOIQSb1";
        String url = "jdbc:mysql://192.168.207.23:8066/otp_addr?useUnicode=true&characterEncoding=utf-8";
        String driver = "com.mysql.jdbc.Driver";
        List<AddressInfo> list = new ArrayList<>();

        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        //String sql = "SELECT waybill_no FROM t_oms_order WHERE  order_create_time >= '2019-07-31 13:00:01.000' AND order_create_time <= '2019-07-31 16:59:59.000'";
        String sql = "SELECT std_address,province,city,county FROM t_addr_address_std limit 140000";
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
            pst = conn.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pst.setFetchSize(Integer.MIN_VALUE);
            log.info("SQL : {}", sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                AddressInfo addressInfo = new AddressInfo();
                addressInfo.setAddress(rs.getString("std_address").trim());
                addressInfo.setProvince(rs.getString("province").trim());
                addressInfo.setCity(rs.getString("city").trim());
                String county = rs.getString("county").trim();
                addressInfo.setCounty(county);
                list.add(addressInfo);
                if (StringUtils.isEmpty(addressInfo.getCounty())){
                    list.remove(addressInfo);
                }

            }
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                rs.close();
                pst.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public void imports(){
        String filePath = "F:\\承诺达\\地址库\\收发件地址.xls";
        Long start = System.currentTimeMillis();
        List<Object> objects = ExcelUtil.readExcel(filePath, OrderInfo.class, ExcelTypeEnum.XLS);

        Object object = null;
        for (int i = 0; i < objects.size(); i++) {
            object = objects.get(i).toString().trim();
            System.out.println("object " + i + ":" + object);
        }
        Long end = System.currentTimeMillis();

        System.out.println("excel data size:" + objects.size());

        System.out.println("cost time:" + (end - start) + " ms");

//        Long start = System.currentTimeMillis();
//        List<Object> objects = ExcelUtil.readMoreThan1000RowBySheet(filePath);
//        Object object = null;
//        for (int i = 0;i<objects.size();i++){
//            object = objects.get(i).toString().trim();
//            System.out.println(object);
//        }
//        Long end = System.currentTimeMillis();
//        System.out.println("cost time:"+(end-start));
    }

    public void export() {
        Long start = System.currentTimeMillis();
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setRecipientProvName("a");
        orderInfo.setRecipientCityName("a");
        orderInfo.setRecipientAreaName("a");
        orderInfo.setRecipientAddress("a");
        orderInfo.setSenderProvName("a");
        orderInfo.setSenderCityName("a");
        orderInfo.setSenderAreaName("a");
        orderInfo.setSenderAddress("a");
        orderInfo.setCellStyleMap(new HashMap<Integer,CellStyle>());


        List<OrderInfo> datas =new ArrayList<>();
        datas.add(orderInfo);

        OutputStream out = null;
        try {
            out = new FileOutputStream("C:\\Users\\lyk\\Desktop\\性能测试\\08-15-log\\test.xlsx");
            Sheet sheet = new Sheet(1, 1, OrderInfo.class);
            sheet.setSheetName("Sheet1");
            ExcelUtil.writeExcel(out, datas, ExcelTypeEnum.XLSX, sheet);
            log.info("导出{}行成功！", datas.size());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Long end = System.currentTimeMillis();
        System.out.println("cost time:"+(end-start));
    }


    public static void main(String[] args) {
        Test test = new Test();
//        test.getAddresss();
//        System.out.println(test.getAddresss());
        test.export();
    }

}
