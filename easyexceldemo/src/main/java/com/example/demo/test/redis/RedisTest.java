package com.example.demo.test.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class RedisTest {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private RedisUtils redisUtils;
    @RequestMapping("/addList")
    public void addList() {
        redisTemplate.opsForList().rightPush("list", "one");
        redisTemplate.opsForList().rightPush("list", "two");
        redisTemplate.opsForList().rightPush("list", "three");
    }

    @RequestMapping("/getList")
    public List<Object> getList() {

        List<String> list1 = redisTemplate.opsForList().range("list",0,-1);
        log.info("List1:{}", list1);
        List<Object> list2 = redisUtils.getList("list",0,-1);
        log.info("List2:{}", list2);
        System.out.println(redisTemplate.opsForList().size("list"));
        System.out.println(redisUtils.getListSize("list"));
        return list2;
    }
}
