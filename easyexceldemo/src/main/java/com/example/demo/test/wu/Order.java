/*
 * Copyright © 2015-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.demo.test.wu;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;


/**
 * 订单实体.
 * @author Jm
 * @since 0.0.1
 */

public class Order implements Serializable {

	private static final long serialVersionUID = 2910805857963869615L;
	private Integer autoId;
	// 订单�?(不唯�?)
	private String orderNo;
	// 订单渠道代码
	private String orderChannelCode;
	// 客户编码
	private String customerCode;
	// 客户秘钥
	private String customerSecretKey;
	// 订单业务类型（同城当天�?�跨城次晨）
	private String orderBussinessTypeCode;

	private String orderBussinessTypeName;
	// 订单状�??(01-已下单，02-分单成功�?03-分单失败�?04-已取消，05-已取件，07-取件失败<业务员取件失败，客户不寄>)
	private String orderStatus;
	// 运单�?
	private String waybillNo;
	// 订单物流号（唯一�?
	private String orderLogisticsCode;
	// 订单创建时间
	private Timestamp orderCreateTime;
	// 订单更新时间
	private Timestamp orderModifyTime;
	// 客户下单时间
	private String custOrderCreateTime;
	// 订单取消时间
	private Timestamp orderCancelTime;
	// 订单取消�?
	private String orderCancelEmpCode;
	// 预约揽收时间
	private String takingAppointmentTime;
	// 预约派件时间
	private String deliveryAppointmentTime;
	// 发件�?
	private String senderName;
	// 发件人手�?
	private String senderMobile;
	// 发件人电�?
	private String senderPhone;
	// 发件人邮�?
	private String senderPostalCode;
	// 发件人省份Code（国标）
	private String senderProvCode;
	// 发件人省份Name
	private String senderProvName;
	// 发件人城市Code（国标）
	private String senderCityCode;
	// 发件人城�?
	private String senderCityName;
	// 发件人区县Code（国标）
	private String senderAreaCode;
	// 发件人区�?
	private String senderAreaName;
	// 发件人乡镇Code（国标）
	private String senderTownCode;
	// 发件人乡�?
	private String senderTownName;
	// 发件人详细地�?
	private String senderAddress;
	// 收件人姓�?
	private String recipientName;
	// 收件人手�?
	private String recipientMobile;
	// 收件人电�?
	private String recipientPhone;
	// 收件人邮�?
	private String recipientPostalCode;
	// 收件人省份Code（国标）
	private String recipientProvCode;
	// 收件人省份Name
	private String recipientProvName;
	// 收件人城市Code（国标）
	private String recipientCityCode;
	// 收件人城�?
	private String recipientCityName;
	// 收件人区县Code（国标）
	private String recipientAreaCode;
	// 收件人区县Name
	private String recipientAreaName;
	// 收件人乡镇Code（国标）
	private String recipientTownCode;
	// 收件人乡镇Name
	private String recipientTownName;
	// 收件人详细地�?
	private String recipientAddress;
	// 增�?�服务类�?
	private String incrementFlag;
	// 运费
	private Double freightFee;
	// 重量
	private Double weight;
	// 物品中金�?
	private Double goodsTotalFee;
	// 物品类型
	private String goodsType;
	// 物品名称
	private String goodsName;
	// 特殊物品类型
	private String specialGoodsType;
	// 包裹数量（如果是�?单多件，表示数量3�?
	private Integer packageNum;
	// 备注
	private String remark;
	// 仓库编码
	private String warehouseCode;
	// 仓库地址
	private String warehouseAddress;
	// 抵用券类�?
	private String couponsType;
	// 抵用券金�?
	private Double couponsFee;
	// 异常反馈
	private String abnormalCode;
	// 付款方式:现金,网络支付,银行�?
	private String payType;
	// 付款对象:寄付,到付,第三方付
	private String payObject;
	// 揽收业务�?
	private String takingEmpCode;
	// 增�?�服�?
	private List<IncrementService> incrementService;
	// 催件次数
	private Integer urgeCount;

	private String assignOrgCode;// 预分配网点Code

	private String assignOrgName;// 预分配网点name

	private String assignEmpCode;// 预分配业务员Code

	private String assignEmpName;// 预分配业务员name

	private Timestamp assignTime;// 预分配业务员时间

	// 结算方式:S1现结,S2月结
	private String settlementType;

	private String packageSize;// 包装大小

	private String totalWeight;// 订单总重�?
	private Timestamp expectTakeTime;//业务员预计取件时�?
	private  String orderStatusName;//订单状�?�描�?
	
	//第三方付款账�?
	private String thirdPayAccount;
	
	//取件类型 （是否需要分单）
	private String takeOrderType;

	public Integer getAutoId() {
		return autoId;
	}

	public void setAutoId(Integer autoId) {
		this.autoId = autoId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrderChannelCode() {
		return orderChannelCode;
	}

	public void setOrderChannelCode(String orderChannelCode) {
		this.orderChannelCode = orderChannelCode;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerSecretKey() {
		return customerSecretKey;
	}

	public void setCustomerSecretKey(String customerSecretKey) {
		this.customerSecretKey = customerSecretKey;
	}

	public String getOrderBussinessTypeCode() {
		return orderBussinessTypeCode;
	}

	public void setOrderBussinessTypeCode(String orderBussinessTypeCode) {
		this.orderBussinessTypeCode = orderBussinessTypeCode;
	}

	public String getOrderBussinessTypeName() {
		return orderBussinessTypeName;
	}

	public void setOrderBussinessTypeName(String orderBussinessTypeName) {
		this.orderBussinessTypeName = orderBussinessTypeName;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getWaybillNo() {
		return waybillNo;
	}

	public void setWaybillNo(String waybillNo) {
		this.waybillNo = waybillNo;
	}

	public String getOrderLogisticsCode() {
		return orderLogisticsCode;
	}

	public void setOrderLogisticsCode(String orderLogisticsCode) {
		this.orderLogisticsCode = orderLogisticsCode;
	}

	public Timestamp getOrderCreateTime() {
		return orderCreateTime;
	}

	public void setOrderCreateTime(Timestamp orderCreateTime) {
		this.orderCreateTime = orderCreateTime;
	}

	public Timestamp getOrderModifyTime() {
		return orderModifyTime;
	}

	public void setOrderModifyTime(Timestamp orderModifyTime) {
		this.orderModifyTime = orderModifyTime;
	}

	public String getCustOrderCreateTime() {
		return custOrderCreateTime;
	}

	public void setCustOrderCreateTime(String custOrderCreateTime) {
		this.custOrderCreateTime = custOrderCreateTime;
	}

	public Timestamp getOrderCancelTime() {
		return orderCancelTime;
	}

	public void setOrderCancelTime(Timestamp orderCancelTime) {
		this.orderCancelTime = orderCancelTime;
	}

	public String getOrderCancelEmpCode() {
		return orderCancelEmpCode;
	}

	public void setOrderCancelEmpCode(String orderCancelEmpCode) {
		this.orderCancelEmpCode = orderCancelEmpCode;
	}

	public String getTakingAppointmentTime() {
		return takingAppointmentTime;
	}

	public void setTakingAppointmentTime(String takingAppointmentTime) {
		this.takingAppointmentTime = takingAppointmentTime;
	}

	public String getDeliveryAppointmentTime() {
		return deliveryAppointmentTime;
	}

	public void setDeliveryAppointmentTime(String deliveryAppointmentTime) {
		this.deliveryAppointmentTime = deliveryAppointmentTime;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderMobile() {
		return senderMobile;
	}

	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}

	public String getSenderPhone() {
		return senderPhone;
	}

	public void setSenderPhone(String senderPhone) {
		this.senderPhone = senderPhone;
	}

	public String getSenderPostalCode() {
		return senderPostalCode;
	}

	public void setSenderPostalCode(String senderPostalCode) {
		this.senderPostalCode = senderPostalCode;
	}

	public String getSenderProvCode() {
		return senderProvCode;
	}

	public void setSenderProvCode(String senderProvCode) {
		this.senderProvCode = senderProvCode;
	}

	public String getSenderProvName() {
		return senderProvName;
	}

	public void setSenderProvName(String senderProvName) {
		this.senderProvName = senderProvName;
	}

	public String getSenderCityCode() {
		return senderCityCode;
	}

	public void setSenderCityCode(String senderCityCode) {
		this.senderCityCode = senderCityCode;
	}

	public String getSenderCityName() {
		return senderCityName;
	}

	public void setSenderCityName(String senderCityName) {
		this.senderCityName = senderCityName;
	}

	public String getSenderAreaCode() {
		return senderAreaCode;
	}

	public void setSenderAreaCode(String senderAreaCode) {
		this.senderAreaCode = senderAreaCode;
	}

	public String getSenderAreaName() {
		return senderAreaName;
	}

	public void setSenderAreaName(String senderAreaName) {
		this.senderAreaName = senderAreaName;
	}

	public String getSenderTownCode() {
		return senderTownCode;
	}

	public void setSenderTownCode(String senderTownCode) {
		this.senderTownCode = senderTownCode;
	}

	public String getSenderTownName() {
		return senderTownName;
	}

	public void setSenderTownName(String senderTownName) {
		this.senderTownName = senderTownName;
	}

	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	public String getRecipientName() {
		return recipientName;
	}

	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	public String getRecipientMobile() {
		return recipientMobile;
	}

	public void setRecipientMobile(String recipientMobile) {
		this.recipientMobile = recipientMobile;
	}

	public String getRecipientPhone() {
		return recipientPhone;
	}

	public void setRecipientPhone(String recipientPhone) {
		this.recipientPhone = recipientPhone;
	}

	public String getRecipientPostalCode() {
		return recipientPostalCode;
	}

	public void setRecipientPostalCode(String recipientPostalCode) {
		this.recipientPostalCode = recipientPostalCode;
	}

	public String getRecipientProvCode() {
		return recipientProvCode;
	}

	public void setRecipientProvCode(String recipientProvCode) {
		this.recipientProvCode = recipientProvCode;
	}

	public String getRecipientProvName() {
		return recipientProvName;
	}

	public void setRecipientProvName(String recipientProvName) {
		this.recipientProvName = recipientProvName;
	}

	public String getRecipientCityCode() {
		return recipientCityCode;
	}

	public void setRecipientCityCode(String recipientCityCode) {
		this.recipientCityCode = recipientCityCode;
	}

	public String getRecipientCityName() {
		return recipientCityName;
	}

	public void setRecipientCityName(String recipientCityName) {
		this.recipientCityName = recipientCityName;
	}

	public String getRecipientAreaCode() {
		return recipientAreaCode;
	}

	public void setRecipientAreaCode(String recipientAreaCode) {
		this.recipientAreaCode = recipientAreaCode;
	}

	public String getRecipientAreaName() {
		return recipientAreaName;
	}

	public void setRecipientAreaName(String recipientAreaName) {
		this.recipientAreaName = recipientAreaName;
	}

	public String getRecipientTownCode() {
		return recipientTownCode;
	}

	public void setRecipientTownCode(String recipientTownCode) {
		this.recipientTownCode = recipientTownCode;
	}

	public String getRecipientTownName() {
		return recipientTownName;
	}

	public void setRecipientTownName(String recipientTownName) {
		this.recipientTownName = recipientTownName;
	}

	public String getRecipientAddress() {
		return recipientAddress;
	}

	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}

	public String getIncrementFlag() {
		return incrementFlag;
	}

	public void setIncrementFlag(String incrementFlag) {
		this.incrementFlag = incrementFlag;
	}

	public Double getFreightFee() {
		return freightFee;
	}

	public void setFreightFee(Double freightFee) {
		this.freightFee = freightFee;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getGoodsTotalFee() {
		return goodsTotalFee;
	}

	public void setGoodsTotalFee(Double goodsTotalFee) {
		this.goodsTotalFee = goodsTotalFee;
	}

	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getSpecialGoodsType() {
		return specialGoodsType;
	}

	public void setSpecialGoodsType(String specialGoodsType) {
		this.specialGoodsType = specialGoodsType;
	}

	public Integer getPackageNum() {
		return packageNum;
	}

	public void setPackageNum(Integer packageNum) {
		this.packageNum = packageNum;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getWarehouseCode() {
		return warehouseCode;
	}

	public void setWarehouseCode(String warehouseCode) {
		this.warehouseCode = warehouseCode;
	}

	public String getWarehouseAddress() {
		return warehouseAddress;
	}

	public void setWarehouseAddress(String warehouseAddress) {
		this.warehouseAddress = warehouseAddress;
	}

	public String getCouponsType() {
		return couponsType;
	}

	public void setCouponsType(String couponsType) {
		this.couponsType = couponsType;
	}

	public Double getCouponsFee() {
		return couponsFee;
	}

	public void setCouponsFee(Double couponsFee) {
		this.couponsFee = couponsFee;
	}

	public String getAbnormalCode() {
		return abnormalCode;
	}

	public void setAbnormalCode(String abnormalCode) {
		this.abnormalCode = abnormalCode;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getPayObject() {
		return payObject;
	}

	public void setPayObject(String payObject) {
		this.payObject = payObject;
	}

	public String getTakingEmpCode() {
		return takingEmpCode;
	}

	public void setTakingEmpCode(String takingEmpCode) {
		this.takingEmpCode = takingEmpCode;
	}

	public List<IncrementService> getIncrementService() {
		return incrementService;
	}

	public void setIncrementService(List<IncrementService> incrementService) {
		this.incrementService = incrementService;
	}

	public Integer getUrgeCount() {
		return urgeCount;
	}

	public void setUrgeCount(Integer urgeCount) {
		this.urgeCount = urgeCount;
	}

	public String getAssignOrgCode() {
		return assignOrgCode;
	}

	public void setAssignOrgCode(String assignOrgCode) {
		this.assignOrgCode = assignOrgCode;
	}

	public String getAssignOrgName() {
		return assignOrgName;
	}

	public void setAssignOrgName(String assignOrgName) {
		this.assignOrgName = assignOrgName;
	}

	public String getAssignEmpCode() {
		return assignEmpCode;
	}

	public void setAssignEmpCode(String assignEmpCode) {
		this.assignEmpCode = assignEmpCode;
	}

	public String getAssignEmpName() {
		return assignEmpName;
	}

	public void setAssignEmpName(String assignEmpName) {
		this.assignEmpName = assignEmpName;
	}

	public Timestamp getAssignTime() {
		return assignTime;
	}

	public void setAssignTime(Timestamp assignTime) {
		this.assignTime = assignTime;
	}

	public String getSettlementType() {
		return settlementType;
	}

	public void setSettlementType(String settlementType) {
		this.settlementType = settlementType;
	}

	public String getPackageSize() {
		return packageSize;
	}

	public void setPackageSize(String packageSize) {
		this.packageSize = packageSize;
	}

	public String getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}

	public Timestamp getExpectTakeTime() {
		return expectTakeTime;
	}

	public void setExpectTakeTime(Timestamp expectTakeTime) {
		this.expectTakeTime = expectTakeTime;
	}

	public String getOrderStatusName() {
		return orderStatusName;
	}

	public void setOrderStatusName(String orderStatusName) {
		this.orderStatusName = orderStatusName;
	}

	public String getThirdPayAccount() {
		return thirdPayAccount;
	}

	public void setThirdPayAccount(String thirdPayAccount) {
		this.thirdPayAccount = thirdPayAccount;
	}
    
	public String getTakeOrderType() {
		return takeOrderType;
	}

	public void setTakeOrderType(String takeOrderType) {
		this.takeOrderType = takeOrderType;
	}
	
	
}
