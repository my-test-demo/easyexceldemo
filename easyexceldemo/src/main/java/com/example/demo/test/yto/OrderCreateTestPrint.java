package com.example.demo.test.yto;


import com.alibaba.fastjson.JSON;
import com.example.demo.test.excel.OrderInfo;
import com.example.demo.test.wu.IncrementService;
import com.example.demo.test.wu.Order;
import org.apache.commons.codec.binary.Base64;

import javax.validation.constraints.Max;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class OrderCreateTestPrint {

    public static void main(String[] args) {
    	OrderCreateTestPrint order=new OrderCreateTestPrint(); 
    	String content=order.refresh();
    	String result=order.execute(content);
    	System.out.println(result);
    	
    }

    /**
     * 刷新数据
     * @return
     */
    public String refresh(){

    	String content = JSON.toJSONString(createOrder());
    	return content;
    	   
       }

    /**
     * 执行用例
     * @param content
     * @return
     */
    public String execute(String content){
    	
    	String orderChannelCode = "OTP_WX";//测试渠道
        String orderChannelSecretKey = "k3ze42n9e7tngq19";
    	 String dataSign = doSign(content , "utf-8", orderChannelSecretKey);
         System.out.println(content);
         String line = "";
         String result = "";
         URL url = null;
 		PrintWriter out = null;	                  
 		//String urlstr = "http://10.129.221.182:8082/order/v1/createOrder";
 		String urlstr = "http://10.129.221.182:8082/order/v1/createOrderAndPrint";

 		try {
 			String target = "logisticProviderId="+URLEncoder.encode(orderChannelCode, "UTF-8")+"&logisticsInterface="+ URLEncoder.encode(content, "UTF-8")+ "&dataDigest=" +URLEncoder.encode(dataSign, "UTF-8");
 			System.out.println(URLEncoder.encode(content, "UTF-8"));
 			System.out.println(target);
 			url = new URL(urlstr);
 			URLConnection conn = url.openConnection();
 			conn.setRequestProperty("accept", "*/*");
 	        conn.setRequestProperty("connection", "Keep-Alive");
 	        conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
 	        conn.setDoOutput(true);
 	        conn.setDoInput(true);
 			out = new PrintWriter(conn.getOutputStream());
 			out.print(target.toString());
 			out.flush();
 			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
 			while((line = in.readLine()) != null) {
 	        	result += line;
 	        }
 			in.close();
 			//System.out.println(result);
 		} catch (UnsupportedEncodingException e) {
 			e.printStackTrace();
 		} catch (MalformedURLException e) {
 			e.printStackTrace();
 		} catch (IOException e) {
 			e.printStackTrace();
 		}  
    	return result;
    	   
       }
    
    public static String doSign(String content, String charset, String keys) {
        String sign = "";
        try {
        	content = content + keys;
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(content.getBytes(charset));
            sign = new String(Base64.encodeBase64(md.digest()), charset);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return sign;
    }

    
    public static Order createOrder() {
        Order order = new Order();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      /*  order.setCustomerCode("K21000869");
        order.setCustomerSecretKey("WILz78gFNINyeLQvKK+LBw==");*/
        order.setCustomerCode("K21004151");
        order.setCustomerSecretKey("6vb2EN1c");
//        order.setCustomerCode("K21000119");
//        order.setCustomerSecretKey("WILz78gFNINyeLQvKK+LBw==");
 //       order.setCustomerCode("K210339447");  
 //       order.setCustomerSecretKey("peH6wrzA");
        order.setOrderChannelCode("OTP_WX");
        order.setOrderLogisticsCode("WXTEST_WAYBILL"+Long.toString(System.currentTimeMillis()));
        order.setCustOrderCreateTime(df.format(new Date()));
//        order.setPayObject("");
       order.setOrderBussinessTypeCode("7050");
        order.setOrderBussinessTypeName("承诺达隔日达");
        order.setWeight(0.0);
        order.setGoodsTotalFee(1000.0);
        order.setGoodsType("G1");
        order.setGoodsName("手机,充电");
        order.setPackageNum(1);           //一票多件的数量在这里修改
        order.setPayObject("POB001");    //PDB001 在订单中寄付的值，PDB002，代表到付的意思
        order.setSettlementType("S002");
        
        List<IncrementService> incrementServiceList = new ArrayList<IncrementService>();
//        IncrementService inc = new IncrementService();
//        inc.setIncrementCode("I001");//I001保价  --I002代收货款--I003到付
//        inc.setIncrementName("保价");
//        inc.setIncrementAmt(501.01);
//        incrementServiceList.add(inc);
//    到付代码    
//      IncrementService inc2 = new IncrementService();
//      inc2.setIncrementCode("I003");
//      inc2.setIncrementName("到付");
//      inc2.setIncrementAmt(131.01);
//      incrementServiceList.add(inc2);
//        
//        IncrementService inc3 = new IncrementService();
//        inc3.setIncrementCode("I002");
//        inc3.setIncrementName("代收货款");
//        inc3.setIncrementAmt(1321.01);
//        incrementServiceList.add(inc3);
        
        
        order.setIncrementService(incrementServiceList);
        
//        order.setRecipientProvCode("310000");
        order.setRecipientProvName("上海");
//        order.setRecipientCityCode("310100");
        order.setRecipientCityName("上海�?");
//        order.setRecipientAreaCode("310101");
        order.setRecipientAreaName("静安�?");
        order.setRecipientTownName("");
        order.setRecipientAddress("第一中心小学");
        order.setRecipientMobile("18221931851");
        order.setRecipientPhone("021-53572753");
        order.setRecipientName("李四");
        order.setRemark("试单");
//        order.setSenderProvCode("310000");
        order.setSenderProvName("上海");
//        order.setSenderCityCode("310100");
        order.setSenderCityName("上海�?");
//        order.setSenderAreaCode("310118");
        order.setSenderAreaName("静安�?");
//        order.setSenderTownCode("310118107");
        order.setSenderTownName("");
        order.setSenderAddress("兴业银行(上海支行)");//华中大道
        order.setSenderMobile("18521572340");
        order.setSenderPhone("021-78541288");
        order.setSenderName("张三");
        order.setTakeOrderType("T002");
        return order;
    }

    public static Order createOrder(List<OrderInfo> orderInfoList) {


        Order order = new Order();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        order.setCustomerCode("K21004151");
        order.setCustomerSecretKey("6vb2EN1c");
        order.setOrderChannelCode("OTP_WX");
        order.setOrderLogisticsCode("WXTEST_WAYBILL"+Long.toString(System.currentTimeMillis()));
        order.setCustOrderCreateTime(df.format(new Date()));
        order.setOrderBussinessTypeCode("7050");
        order.setOrderBussinessTypeName("承诺达隔日达");
        order.setWeight(0.0);
        order.setGoodsTotalFee(1000.0);
        order.setGoodsType("G1");
        order.setGoodsName("手机,充电");
        order.setPackageNum(1);           //一票多件的数量在这里修改
        order.setPayObject("POB001");    //PDB001 在订单中寄付的值，PDB002，代表到付的意思
        order.setSettlementType("S002");

        List<IncrementService> incrementServiceList = new ArrayList<IncrementService>();
        order.setIncrementService(incrementServiceList);

        order.setRecipientProvName("上海");
        order.setRecipientCityName("上海�?");
        order.setRecipientAreaName("静安�?");
        order.setRecipientTownName("");
        order.setRecipientAddress("第一中心小学");
        order.setRecipientMobile("18221931851");
        order.setRecipientPhone("021-53572753");
        order.setRecipientName("李四");
        order.setRemark("试单");
        order.setSenderProvName("上海");
        order.setSenderCityName("上海�?");
        order.setSenderAreaName("静安�?");
        order.setSenderTownName("");
        order.setSenderAddress("兴业银行(上海支行)");
        order.setSenderMobile("18521572340");
        order.setSenderPhone("021-78541288");
        order.setSenderName("张三");
        order.setTakeOrderType("T002");
        return order;
    }

}
